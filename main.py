from World import World
from time import time


if __name__ == "__main__":
    gamma = .9
    env = World()
    epsilon = 159.090909090909
    alpha = 0.0320408163265306
    tick = time()
    Q = env.sarsa(10000, alpha, lambda k: 1 / (k * epsilon ** -1 + 1), gamma)
    tok = time()
    print(f'SARSA finished in {tok-tick} seconds')
    env.plot_actionValues(Q)
    epsilon = 363.636363636363
    alpha = 0.01
    tick = time()
    Q = env.Qlearning(10000, alpha, lambda k: 1 / (k * epsilon ** -1 + 1), gamma)
    tok = time()
    print(f'Q-learning finished in {tok - tick} seconds')
    env.plot_actionValues(Q)


